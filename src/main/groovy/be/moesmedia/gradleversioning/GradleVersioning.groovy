package be.moesmedia.gradleversioning

import org.gradle.api.Plugin
import org.gradle.api.Project

class GradleVersioning implements Plugin<Project> {

    private final static String BUILD = "BUILD"
    private final static String MINOR = "MINOR"
    private final static String PATCH = "PATCH"
    private final static String MAJOR = "MAJOR"

    @Override
    void apply(Project target){
        File versionPropsFile = new File("version.properties")
        Properties versionProps = null
        def checkFile = { ->
            if(versionPropsFile.canRead())
                return

            Properties properties = new Properties()
            properties.setProperty(BUILD, "0")
            properties.setProperty(PATCH, "0")
            properties.setProperty(MINOR, "0")
            properties.setProperty(MAJOR, "0")

            properties.store(versionPropsFile.newWriter(), null)
        }

        def getProperties = { ->
            if(versionProps != null)
                return versionProps

            checkFile()
            versionProps = new Properties()
            versionProps.load(versionPropsFile.newReader())
            return versionProps
        }

        def getVersion = { ->
            checkFile();
            Properties v = getProperties();
            return "${v[MAJOR]}.${v[MINOR]}.${v[PATCH]}.${v[BUILD]}"
        }

        def bump = target.tasks.create('bump') << {
            checkFile()
            getProperties().store(versionPropsFile.newWriter(), null)
            target.version = getVersion()
        }
        bump.group = 'gradle-versioning'
        bump.description = 'Writes the new version to file'

        def buildNumber = target.tasks.create('buildnumber') << {
            Properties v = getProperties()
            def buildNumber = v[BUILD].toInteger() + 1
            v[BUILD] = buildNumber.toString()
            versionProps = v
        }
        buildNumber.group = 'gradle-versioning'
        buildNumber.description = 'Sets the last number x.x.x.X one tick higher'

        def patch = target.tasks.create('patch') << {
            Properties v = getProperties()
            def patchNumber = v[PATCH].toInteger() + 1
            v[PATCH] = patchNumber.toString()
            v[BUILD] = "0"
            versionProps = v
        }
        patch.group = 'gradle-versioning'
        patch.description = 'Sets the patch number x.x.X.x one tick higher, resets build number'

        def minor = target.tasks.create('minor') << {
            Properties v = getProperties()
            def minorNumber = v[MINOR].toInteger() + 1
            v[MINOR] = minorNumber.toString()
            v[BUILD] = "0"
            v[PATCH] = "0"
            versionProps = v
        }

        minor.group = 'gradle-versioning'
        minor.description = 'Sets the minor number x.X.x.x one tick higher, resets patch and build number'

        def major = target.tasks.create('major') << {
            Properties v = getProperties()
            def patchNumber = v[MAJOR].toInteger() + 1
            v[MAJOR] = patchNumber.toString()
            v[BUILD] = "0"
            v[PATCH] = "0"
            v[MINOR] = "0"
            versionProps = v
        }

        major.group = 'gradle-versioning'
        major.description = 'Sets the major number X.x.x.x one tick higher, resets minor, patch and build number'

        bump.mustRunAfter(buildNumber)
        bump.mustRunAfter(patch)
        bump.mustRunAfter(minor)
        bump.mustRunAfter(major)

        target.version = getVersion()
    }
}
